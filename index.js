const express = require("express");
const port = 4000;
const app = express();
app.use(express.json());

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

// GET /home 


	app.get("/home", (request,response) => {
		response.status(201).send("Welcome to homepage!")
	})


// GET /items retrieve all items in mock database


	app.get("/items", (request,response) => {
		response.send(items);
	})


// DELETE /delete-item remove item from the mock database


	app.delete("/delete-item", (request,response)=>{
		let deletedItem = items.pop()
		response.send(deletedItem)
	})


app.listen(port);